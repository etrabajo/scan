/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ip.vista;

/**
 *
 * @author victor
 */
public class Consulta {

    private int cant_ip;
    private int cant_sub;
    private int cant_equipo;
    private int cant_notacion;
    private Consulta1 obj;
    private sumador objSumador = new sumador();

    public void generar_subredes(int tipo, int notacion, int cant, String ip) {
        switch (tipo) {

            //Notacion
            case 1:
                int increm;
                int octeto;
                System.out.println("cant = " + cant);
                System.out.println("notacion = " + notacion);
                System.out.println("tipo = " + tipo);

                for (sumador col : objSumador.init()) {
                    if (col.getMascara() == notacion) {
                        System.out.println("incremento = " + col.getIncremento());
                        System.out.println("octecto = " + col.getOctecto());
                        increm = col.getIncremento();
                        octeto = col.getOctecto();
                    }
                }
                
                  int[] ipSeparada = null;
                for (int i = 1; i < cant; i++) {
                    System.out.println("i = " + i);
              
                    String cadena = ip;
                    String delimitadores = "[ .,;?!¡¿\'\"\\[\\]]+";
                    String[] palabrasSeparadas = cadena.split(delimitadores);
                  ipSeparada =  new int[palabrasSeparadas.length];
                   // ArrayInt[0] = Integer.valueOf(palabrasSeparadas[0]).intValue(); 
                    for (int j = 0; j < palabrasSeparadas.length; j++) {
                       ipSeparada[j] = Integer.valueOf(palabrasSeparadas[j]);  
                    }
                    System.out.println("palabrasSeparadas = " + palabrasSeparadas.length);

                }
                System.out.println("octeto = " +  ipSeparada );
                for (int i = 0; ipSeparada.length < 10; i++) {
                    System.out.println("ipSeparada[i] = " + ipSeparada[i]);  
                }
                break;

        }
    }

    public Consulta() {
        obj = new Consulta1();
    }

    public int getCant_ip() {
        return cant_ip;
    }

    public void setCant_ip(int cant_ip) {
        this.cant_ip = cant_ip;
    }

    public int getCant_sub() {
        return cant_sub;
    }

    public void setCant_sub(int cant_sub) {
        this.cant_sub = cant_sub;
    }

    public int getCant_equipo() {
        return cant_equipo;
    }

    public void setCant_equipo(int cant_equipo) {
        this.cant_equipo = cant_equipo;
    }

    public int getCant_notacion() {
        return cant_notacion;
    }

    public void setCant_notacion(int cant_notacion) {
        this.cant_notacion = cant_notacion;
    }

    public static void main(String[] args) {
        Consulta on = new Consulta();

        on.generar_subredes(1, 1, 3, "192.168.32.0");
    }

}
