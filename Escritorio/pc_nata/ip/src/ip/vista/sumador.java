/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ip.vista;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author victor
 */
public class sumador {

    int mascara;
    int bits;
    int incremento;
    int octecto;

    

    public sumador() {
    }
    
    

    public List<sumador> init() {
        List<sumador> listSumador = new ArrayList<>();
        sumador obj = new sumador();
        obj.setMascara(32);
        obj.setBits(0);
        obj.setIncremento(1);
        obj.setOctecto(4);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(31);
        obj.setBits(1);
        obj.setIncremento(2);
        obj.setOctecto(4);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(30);
        obj.setBits(2);
        obj.setIncremento(4);
        obj.setOctecto(4);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(29);
        obj.setBits(3);
        obj.setIncremento(8);
        obj.setOctecto(4);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(28);
        obj.setBits(4);
        obj.setIncremento(16);
        obj.setOctecto(4);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(27);
        obj.setBits(5);
        obj.setIncremento(32);
        obj.setOctecto(4);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(26);
        obj.setBits(6);
        obj.setIncremento(64);
        obj.setOctecto(4);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(25);
        obj.setBits(7);
        obj.setIncremento(128);
        obj.setOctecto(4);
        listSumador.add(obj);
        obj = new sumador();

        obj.setMascara(24);
        obj.setBits(0);
        obj.setIncremento(1);
        obj.setOctecto(3);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(23);
        obj.setBits(1);
        obj.setIncremento(2);
        obj.setOctecto(3);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(22);
        obj.setBits(2);
        obj.setIncremento(4);
        obj.setOctecto(3);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(21);
        obj.setBits(3);
        obj.setIncremento(8);
        obj.setOctecto(3);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(20);
        obj.setBits(4);
        obj.setIncremento(16);
        obj.setOctecto(3);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(19);
        obj.setBits(5);
        obj.setIncremento(32);
        obj.setOctecto(3);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(18);
        obj.setBits(6);
        obj.setIncremento(64);
        obj.setOctecto(3);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(17);
        obj.setBits(7);
        obj.setIncremento(128);
        obj.setOctecto(3);
        listSumador.add(obj);
        obj = new sumador();

        obj.setMascara(16);
        obj.setBits(0);
        obj.setIncremento(1);
        obj.setOctecto(2);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(15);
        obj.setBits(1);
        obj.setIncremento(2);
        obj.setOctecto(2);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(14);
        obj.setBits(2);
        obj.setIncremento(4);
        obj.setOctecto(2);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(13);
        obj.setBits(3);
        obj.setIncremento(8);
        obj.setOctecto(2);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(12);
        obj.setBits(4);
        obj.setIncremento(16);
        obj.setOctecto(2);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(11);
        obj.setBits(5);
        obj.setIncremento(32);
        obj.setOctecto(2);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(10);
        obj.setBits(6);
        obj.setIncremento(64);
        obj.setOctecto(2);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(9);
        obj.setBits(7);
        obj.setIncremento(128);
        obj.setOctecto(2);
        listSumador.add(obj);
        obj = new sumador();

        obj.setMascara(8);
        obj.setBits(0);
        obj.setIncremento(1);
        obj.setOctecto(1);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(7);
        obj.setBits(1);
        obj.setIncremento(2);
        obj.setOctecto(1);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(6);
        obj.setBits(2);
        obj.setIncremento(4);
        obj.setOctecto(1);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(5);
        obj.setBits(3);
        obj.setIncremento(8);
        obj.setOctecto(1);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(4);
        obj.setBits(4);
        obj.setIncremento(16);
        obj.setOctecto(1);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(3);
        obj.setBits(5);
        obj.setIncremento(32);
        obj.setOctecto(1);
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(2);
        obj.setBits(6);
        obj.setIncremento(64);
        obj.setOctecto(1);
        
        listSumador.add(obj);
        obj = new sumador();
        obj.setMascara(1);
        obj.setBits(7);
        obj.setIncremento(128);
        obj.setOctecto(1);
         listSumador.add(obj);
         return listSumador;

    }

    public int getMascara() {
        return mascara;
    }

    public void setMascara(int mascara) {
        this.mascara = mascara;
    }

    public int getBits() {
        return bits;
    }

    public void setBits(int bits) {
        this.bits = bits;
    }

    public int getIncremento() {
        return incremento;
    }

    public void setIncremento(int incremento) {
        this.incremento = incremento;
    }

    public int getOctecto() {
        return octecto;
    }

    public void setOctecto(int octecto) {
        this.octecto = octecto;
    }


}
