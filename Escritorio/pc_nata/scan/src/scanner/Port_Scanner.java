/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scanner;

import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author ngrajales
 */
public final class Port_Scanner extends Frame {

    static boolean insufficiente_date_for_scan = false;
    Socket sock;
    int port_start;
    int port_end;
    int portStart;
    int portEnd;
    String target;

    //Creacion del Formulario
    Frame objFrame = new Frame("Escanecador de puertos");
    JLabel l_title = new JLabel("Puesto Escaneado", JLabel.CENTER);
    JLabel l_target = new JLabel("Target:");
    JLabel l_port_start = new JLabel("Puerto Inicial");
    JLabel l_port_end = new JLabel("Puerto Final");
    JLabel l_status = new JLabel("Para iniciar clic en escanear");

    JTextField t_target = new JTextField(15);
    JTextField t_port_start = new JTextField(15);
    JTextField t_port_end = new JTextField(15);

    JTextArea a_mensaje = new JTextArea();

    JScrollPane sp_mensaje = new JScrollPane();

    JButton b_scaneador = new JButton("Escanear");
    JButton b_limpiar = new JButton("Limpiar");

    /**
     * Metodo Utilizado para iniciar la aplicacion
     *
     * @param args
     */
//    public static void main(String[] args) {
//        Port_Scanner port_Scanner = new Port_Scanner();
//        port_Scanner.init();
//    }

    public void init() {
        System.out.println("this = ");
        objFrame.setLayout(new GridLayout(5, 1));
        objFrame.setSize(180, 320);
        objFrame.setResizable(false);

        Panel p_init = new Panel(new GridLayout(3, 3));

        l_title.setForeground(Color.BLACK);
        l_title.setFont(new Font("TimesRoman", Font.BOLD, 20));
        objFrame.add(l_title);

        l_target.setForeground(Color.GRAY);
        p_init.add(l_target);

        t_target.setForeground(Color.gray);
        t_target.setFocusable(true);
        t_target.requestFocus();
        p_init.add(t_target);

        l_port_start.setForeground(Color.black);
        p_init.add(l_port_start);

        t_port_start.setForeground(Color.black);
        t_target.setFocusable(true);
        p_init.add(t_port_start);

        l_port_end.setForeground(Color.black);
        p_init.add(l_port_end);

        l_port_end.setForeground(Color.black);
        l_port_end.setFocusable(true);
        p_init.add(l_port_end);

        objFrame.add(p_init);

        // Panel Siguiente
        Panel p_panelInfo = new Panel();
        JLabel spacio = new JLabel();
        p_panelInfo.add(b_scaneador);
        p_panelInfo.add(spacio);
        p_panelInfo.add(b_limpiar);
        objFrame.add(p_panelInfo);

        //Mensajes
        sp_mensaje.setViewportView(a_mensaje);
        sp_mensaje.setHorizontalScrollBarPolicy(
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        sp_mensaje.setVerticalScrollBarPolicy(
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        objFrame.add(sp_mensaje);

        l_status.setForeground(Color.black);
        objFrame.add(l_status);

        //Lllamado metodo 
    }

    public void add_event_handlers() {

        objFrame.addWindowFocusListener(new WindowAdapter() {
            public void cerrar_ventana(WindowEvent e) {
                System.exit(1);
            }
        });

        b_scaneador.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // 
                accion_escaneador();
            }
        });

        b_limpiar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // 
                accion_limpiar();
            }
        });
    }

    public void accion_escaneador() {
        if (t_target.getText().equals("")) {
            a_mensaje.setText("Faltante en Target");

        } else if (t_port_start.getText().equals("")) {
            a_mensaje.setText("Faltante puerto Inicial");

        } else if (t_port_end.getText().equals("")) {
            a_mensaje.setText("Faltante puerto Final");
        }

        insufficiente_date_for_scan = false;

        a_mensaje.setText("");

        Thread x1 = new Thread() {

            public void run() {
                b_scaneador.setEnabled(false);
                b_limpiar.setText("Parar");

                target = t_target.getText();
                portStart = Integer.parseInt(t_port_start.getText());
                portEnd = Integer.parseInt(t_port_end.getText());

                for (int x = portStart; x <= portEnd; x++) {
                    l_status.setText("Puerto " + x + " Testeado");

                    if (insufficiente_date_for_scan) {
                        break;
                    }

                    try {
                        sock = new Socket(target, x);
                        a_mensaje.append("Puerto " + x + "Abierto \n");
                        sock.close();
                    } catch (Exception e) {
                        a_mensaje.append("Puerto " + x + "Cerrado \n");
                        continue;
                    }

                }
                b_scaneador.setEnabled(true);
                b_limpiar.setText("Limpiar");
                l_status.setText("Presione Escanear..");

            }
        };

        x1.start();

    }

    public void accion_limpiar() {
        insufficiente_date_for_scan = true;
        t_target.setText("");
        t_port_end.setText("");
        t_port_start.setText("");
        a_mensaje.setText("");
        b_limpiar.setText("Limpiar");

    }
}